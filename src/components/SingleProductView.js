/* REACT MODULE */
import {Row, Col, Button, Card} from 'react-bootstrap';
import {useParams} from 'react-router-dom';
import {useEffect, useState, useContext} from 'react';

/* MANUALLY DEFINED PAGE */
import PageNotFound from '../pages/PageNotFound';

/* OTHER REQUIRED MODULE */
import Swal2 from 'sweetalert2'
import UserContext from '../UserContext';


export default function SingleProductView(){

	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');

	const {user} = useContext(UserContext);
	const {id} = useParams();

	/* FUNCTION TO RETRIEVE DETAILS OF SELECTED PRODUCT */
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(response => response.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [])
	/* END FUNCTION RETRIEVE DETAILS */

	return(
		user.isAdmin === false || user.isAdmin === undefined
		?
		<Row>
			<Col>
				<Card>
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Text>
				          {description}
				        </Card.Text>
				        <Card.Text>
				          {price}
				        </Card.Text>
				        <Button variant="primary" >Add to Cart</Button>
				      </Card.Body>
				    </Card>
			</Col>
		</Row>
		:
		<PageNotFound />
	)
}