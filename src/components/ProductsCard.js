/* REACT MODULES */
import { Button, Row, Col, Card, Placeholder } from 'react-bootstrap';	
import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom';

/* OTHER REQUIRED MODULES */
import Swal2 from 'sweetalert2'
import PropTypes from 'prop-types';
import style from '../stylesheets/ProductsCatalogCSS.module.css'


// TO BE DELETED-FOR TESTING
import img1 from '../images/moderntable1.jpg'
import img2 from '../images/moderntable2.jpg'
import img3 from '../images/moderntable3.jpg'
import img4 from '../images/moderntable4.jpg'
import img5 from '../images/moderntable5.jpg'
import img6 from '../images/moderntable6.jpg'
import img7 from '../images/moderntable7.jpg'
import img8 from '../images/moderntable8.jpg'
import img9 from '../images/moderntable9.jpg'


export default function ProductsCard({productsProp}){

    let imgs = [img1, img2, img3, img4, img5, img6, img7, img8, img9];
    const {_id, name, description, price, quantity} = productsProp;

    const [isDisabled, setIsDisabled] = useState();

    const [cart, setCart] = useState([]);
    
    /* FUNCTION TO ADD PRODUCT TO USER CART ARRAY */
    function addToCart() {
        fetch(`${process.env.REACT_APP_API_URL}/cart`, {
            method: 'PATCH',
            headers: {
                'Content-type' : 'application/json',
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                cart: [{
                    productId: _id,
                    quantity: 1
                    }]
            })
        })
        .then(response => response.json())
        .then(data => {
            if(data == true){
                Swal2.fire({
                    title: 'Added to Cart!',
                    icon: 'success'
                })
            }
        })
    }

    // FOR TESTING
    let rand = Math.floor(Math.random() * 9);

	return (
		<Card className={style.colHeight} >
            <Card.Img variant='top' src={imgs[rand]} className={style.cardImg}/>
            <Card.ImgOverlay className={style.cardOverlay}>
                <div className={style.overlayContent}>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text></Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Card.Subtitle>Stock</Card.Subtitle>
                    <Card.Text>{quantity}</Card.Text>
                    <Button as = {Link} to = {`/products/${_id}`} variant="outline-light" disabled={isDisabled}>Check Details</Button>
                    <Button variant="outline-light" disabled={isDisabled} onClick={e => addToCart(_id)}>Add</Button>
                </div>
            </Card.ImgOverlay>
            
        </Card>
	)
}


ProductsCard.propTypes = {
    productsProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}