/* IMPORT REACT MODULES */
import {useState, useEffect} from 'react'
import {Modal, Container, Card, Row, Col, Form, Button, Nav} from 'react-bootstrap'
import Swal2 from 'sweetalert2'

/* FUNCTION TO CHANGE MODAL STATE */
let handleShow = () => {}
export function showModal(data){
	handleShow(data)
}

export default function UpdateProductModal(){

	/* UPDATE PRODUCT FIELDS STATE */
	const [updateProdName, setUpdateProdName] = useState('');
	const [updateProdDesc, setUpdateProdDesc] = useState('');
	const [updateProdPrice, setUpdateProdPrice] = useState('');
	const [updateProdQuantity, setUpdateProdQuantity] = useState('');
	const [updateProdId, setUpdateProdId] = useState('');

	
	/* MODAL FUNCTIONS */
	const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);

    /* FUNCTION TO SHOW MODAL AND PASS PRODUCT DETAILS TO BE UPDATED */
  	handleShow = (data) => {
  		setUpdateProdName(data.name)
  		setUpdateProdDesc(data.description)
  		setUpdateProdPrice(data.price)
  		setUpdateProdQuantity(data.quantity)
  		setUpdateProdId(data._id)
  		setShow(true);
  	}
  	/* END FUNCTION UPDATE PRODUCT MODAL */

  	/* FUNCTION TO UPDATE PRODUCT DETAILS IN DATABASE */
	function updateProduct(prod){
		fetch(`${process.env.REACT_APP_API_URL}/products/${prod}/update` , {
			method: 'PUT',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name : updateProdName,
				description : updateProdDesc,
				price : updateProdPrice,
				quantity : updateProdQuantity
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				handleClose();
				Swal2.fire({
					title: 'Successfully Updated Product!',
					icon: 'success',
					text: 'Check your products.'
				})
			}
		})
	}
	/* END FUNCTION UPDATE PRODUCT DETAILS */

	return(
		<Modal size='xl' show={show} onHide={handleClose} backdrop="static" keyboard={false} centered>
			<Modal.Header closeButton>
			  <Modal.Title>Update Product</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Container>
					<Form>
					<Row>
						<Col xl={3}>
							<Card>
						      <Card.Img variant="top" src="holder.js/100px180" />
						      <Card.Body as={Nav.Link}  style={{maxHeight: '10rem', minHeight: '10rem'}}>
						        <Card.Img />
						      </Card.Body>
						    </Card>
						</Col>						
						<Col xl={4}>
						<Row>
							<Col xl={12}>
								<Form.Group className="mb-3" controlId="updateProdName">
									<Form.Label>Product Name</Form.Label>
									<Form.Control type="text" autoFocus value={updateProdName} onChange={(e) => setUpdateProdName(e.target.value)}/>
								</Form.Group>
							</Col>
						</Row>
						<Row>
							<Col xl={6}>
								<Form.Group className="mb-3" controlId="updateProdPrice">
									<Form.Label>Product Price</Form.Label>
									<Form.Control type="text" value={updateProdPrice} onChange={(e) => setUpdateProdPrice(e.target.value)}/>
								</Form.Group>
							</Col>
							<Col xl={6}>
								<Form.Group className="mb-3" controlId="updateProdQuantity">
									<Form.Label>Product Quantity</Form.Label>
									<Form.Control type="text" value={updateProdQuantity} onChange={(e) => setUpdateProdQuantity(e.target.value)}/>
								</Form.Group>
							</Col>
						</Row>
						</Col>
						<Col xl={5}>
							<Form.Group className="mb-3" controlId="updateProdDesc">
								<Form.Label>Product Description</Form.Label>
								<Form.Control as="textarea" rows={5} value={updateProdDesc} onChange={(e) => setUpdateProdDesc(e.target.value)}/>
							</Form.Group>
						</Col>			
					</Row>
					</Form>
				</Container>
	        </Modal.Body>
			<Modal.Footer>
				<Button variant="outline-success" onClick={e => updateProduct(updateProdId)}>Save</Button>
				<Button variant="outline-danger" onClick={handleClose}>
				Discard Changes
				</Button>
			</Modal.Footer>
		</Modal>
	)
}