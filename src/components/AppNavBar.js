import {Container, Navbar, Nav} from 'react-bootstrap';

import {useState, useEffect, useContext} from 'react';
import {NavLink, Link} from 'react-router-dom';

import UserContext from '../UserContext';

export default function AppNavBar(){

    const {user} = useContext(UserContext);

    // useEffect(() => {
    //     setUser(localStorage.getItem('email'));
    // }, [localStorage.getItem('email')])

	return (
		<Navbar bg="light" expand="lg">
            <Container fluid>
                <Navbar.Brand as = {Link} to = '/'>Brand Logo</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                    <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>

                    {
                        user.id === null || user.id === undefined
                        ?
                        <>
                            <Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
                        </>
                        :
                        user.isAdmin === true 
                        ?
                        <>
                            <Nav.Link as = {NavLink} to = '/adminDashboard'>Admin Dashboard</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/users'>User Management</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/orders'>Orders</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
                        </>
                        :
                        <>
                            <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/cart'>Cart</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/orders'>Orders</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
                        </>
                    }   

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
	)
}