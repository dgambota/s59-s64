// REACT MODULES
import { Fragment } from 'react'
import { Container } from 'react-bootstrap'

// MANUAL DEFINED COMPONENTS
import AppNavBar from './components/AppNavBar'
import {useState, useEffect} from 'react'; 
import {BrowserRouter, Route, Routes} from 'react-router-dom'

// MANUAL DEFINED PAGES
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound'
import AdminDashboard from './pages/AdminDashboard';



import {UserProvider} from './UserContext';
import './App.css';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    
  }, [user])

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  }, [])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar />
        <Container fluid>  
            <Routes>
              <Route path='/' element = {<Home/>} />
              <Route path='/register' element = {<Register/>} />
              <Route path='/adminDashboard' element = {<AdminDashboard />} />
              <Route path='/products' element = {<Products/>} />
              <Route path='/login' element = {<Login/>} />
              <Route path='/products/:id' element = {<SingleProductView/>} />
              <Route path='/logout' element = {<Logout/>} />
              <Route path="*" element={<PageNotFound />} />
            </Routes>
        </Container>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
