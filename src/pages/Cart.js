/* IMPORT REACT MODULES */
import UserContext from '../UserContext';
import {useContext, useEffect, useState} from 'react'
import {Container, Row, Col, Table, Button, Form, Card} from 'react-bootstrap'


export default function Cart(){

	/* FUNCTION TO FILL CART WITH PRODUCTS ADDED */
	let getCart = () => {
		fetch(`${process.env.REACT_APP_API_URL}/cart`, {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if(data.length > 0){
				let total = 0;
				setCart(data.map(item => {
					total += item.subtotal;
					console.log(getProductDetails(item._id))
					return(
						<Col xs={12} md={12} xl={12} >
						    <Card className={style.productCard} key = {item._id}>
						    	<Row >
						    		<Col xl={3} >
						    			<Card.Img variant='top' src={`https://res.cloudinary.com/dilv1kuce/image/upload/c_fill/${item.imgId}.jpg`}/>
						    		</Col>
							        <Col xl={4}>
							        	<Row className="mb-3 mt-3">
							        	<Card.Title>
							        	    <h5>{item.productId}</h5>
							        	</Card.Title>
							        	</Row>
							        	<Row>
							        		<p>Product Description</p>
							        	</Row>
							        </Col>
							        <Col xl={3}>
							        	<Row className="mb-5 mt-3">
							        		<Col xl={12} className="mx-auto">
							        			<p>Price: 88</p>
							        		</Col>
							        	</Row>
							        	<Row>
							        	<Col xl={2}>
						        	    	<Button id={`deduct${item.productId}`} disabled={false} variant='success' >-</Button>
						        	    </Col>
							        	<Col xl={7}>
							        		<Form.Control disabled={false} readOnly id={`quantity${item.productId}`} type="text" value={item.quantity}/>
							        	</Col>
							        	<Col xl={2}>
						        	    	<Button id={`add${item.productId}`} disabled={false} variant='success' onClick={() => updateQuantity(item.productId)}>+</Button>
						        	    </Col>
						        	    </Row>
						            </Col>
						            <Col xl={2}>
						            	<Row className="mb-5 mt-3">
						            		<Col xl={12}>
						            		<h5 type="text" id={`subtotal${item.productId}`}>{item.subtotal}</h5>
						            		</Col>
						            	</Row>
						            	<Row>
						            		<Col xl={12}>
						            		<Button variant='outline-danger' onClick={e => removeFromCart(item.productId)}>Remove</Button>
						            		</Col>
						            	</Row>
						            </Col>

							        
						        </Row>
						    </Card>
						</Col>
					)
				}))
				getTotal();
			} else{
				setCart(
					<tr key = {0}>
						<td colSpan={4}><h1>Your cart is Empty</h1></td>
					</tr>
				)
			}
		})
	}
	/* END FUNCTION FILL CART */
	
		
	/* FUNCTION TO PASS AND EMPTY CART DETAILS AND CREATE NEW ORDER */
	function checkout(){
		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				Swal2.fire({
					title: 'Checkout Successfull!',
					icon: 'success',
					text: 'Your products will be delivered soon.'
				})
			}
			if(data.status === 'empty'){
				Swal2.fire({
					title: 'Your Cart is Empty!',
					icon: 'error',
					text: 'Check our products and add one to cart.'
				})
			}
		})
	}
	/* END FUNCTION CHECKOUT */

	return(
		user.isAdmin === false 
		?
		<Container>
			<Row style={{marginTop: '5rem'}}>
				<Col xl={8} className="mt-5">
					<h2>Items</h2>
					{cart}
				</Col>
				<Col xl={4} style={{marginTop: '6rem'}}>
					<Card className="cardHighlight">
					<Card.Title className="mx-auto mt-2">
						<Row>
							<Col xl={12}>
				            	<h2>Order Details</h2>
					        </Col>
					    </Row>
				    </Card.Title>
				    <Card.Body>
				    <Row className="mx-auto py-1">
				    	<Col xl={6}>
				    		Subtotal:
				    	</Col>
				    	<Col xl={6}>
				    		{total}
				    	</Col>
				    </Row>
				    <Row className="mx-auto py-1">
				    	<Col xl={6}>
				    		Shipping:
				    	</Col>
				    	<Col xl={6}>
				    		$$$
				    	</Col>
				    </Row>
				    <Row className="mx-auto py-1">
				    	<Col xl={6}>
				    		Taxes:
				    	</Col>
				    	<Col xl={6}>
				    		$$$
				    	</Col>
				    </Row>
				    <Row className="mx-auto py-5">
			        	<Col xl={6}>
			        		<h5>Total:</h5>
			        	</Col>
			        	<Col xl={6}>
			        		<h5></h5>
			        	</Col>
				    </Row>
				    <Row>
				    	<Button variant='success' onClick={e => checkout()}>Checkout</Button>
				    </Row>
				    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		:
		<PageNotFound />
	)
}