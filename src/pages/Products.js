/* IMPORT REACT MODULES */
import { Fragment, useEffect, useState, useContext, Link } from 'react';
import { Col, Row, Container, Form, Button, Card, Placeholder } from 'react-bootstrap'

/* IMPORT MANUAL DEFINED COMPONENTS */
import ProductsCard from '../components/ProductsCard';
import UserContext from '../UserContext';
import PageNotFound from '../pages/PageNotFound';
import LoadingPlaceholder from '../components/LoadingPlaceholder';

/* IMPORT STYLESHEET */
import style from '../stylesheets/ProductsCatalogCSS.module.css'


export default function Products(){

	const {user, setUser} = useContext(UserContext);

	const [products, setProducts] = useState([])
	const [searchInput, setSearchInput] = useState('');
	const [searchIsEmpty, setSearchIsEmpty] = useState(true);
	const [isProductsLoading, setIsProductsLoading] = useState(true);

	/* HOOK TO SEARCH PRODUCTS */
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json'
			},
			body: JSON.stringify({
				name: searchInput
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data.length != 0){
				setProducts(data.map(products => {
					return(
						<Col key = {products._id} xl={2} md={3} xs={6} sm={4}>
							<ProductsCard  productsProp = {products} />
						</Col>
					)
				}));
				setSearchIsEmpty(false)
			} else{
				setSearchIsEmpty(true)
			}
		})
	}, [searchInput])
	/* END HOOK SEARCH PRODUCTS */


	/* HOOK GET ACTIVE PRODUCTS */
	useEffect(() => {
		if(searchIsEmpty == true){
			fetch(`${process.env.REACT_APP_API_URL}/products/active`)
			.then(response => response.json())
			.then(data => {
				setIsProductsLoading(false);
				setProducts(data.map(products => {
					return(
						<Col key = {products._id} xl={2} md={3} xs={6} sm={4}>
							<ProductsCard  productsProp = {products} />
						</Col>
					)
				}));
			})
		}
	}, [searchIsEmpty])	
	/* END HOOK GET ACTIVE PRODUCTS */


	return(
		user.isAdmin === true
		?
		<PageNotFound />
		:
		<Container fluid>
			<Form>
				<Row>
					<Col xl={6}>
						<Form.Group className="mb-3" controlId="productName">
						  <Form.Control type="text" placeholder="Enter Product Name" value={searchInput} onChange={e => setSearchInput(e.target.value)}/>
						</Form.Group>
					</Col>
				</Row>
			</Form>
			{
			isProductsLoading ?
			<Row className='gx-1'>
				<LoadingPlaceholder loadPlaceholder={'products'} />
			</Row>
			:
			<Row className='gx-1'>
				{products}
			</Row>
			}
		</Container>
	)
}