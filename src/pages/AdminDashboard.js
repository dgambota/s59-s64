import {Container, Row, Col, Table, Button, Form, Modal, Image, Card} from 'react-bootstrap';
import {useContext, useState, useEffect} from 'react'
import {useParams, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import PageNotFound from '../pages/PageNotFound';
import Swal2 from 'sweetalert2'
import UpdateProductModal, {showModal} from '../components/UpdateProductModal'

export default function AdminDashboard(){

	// Modal functions
	const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);

  	const [showAddModal, setShowAddModal] = useState(false);
    const closeAddProdModal = () => setShowAddModal(false);
  	const showAddProdModal = () => setShowAddModal(true);


	const {user} = useContext(UserContext);

	// Get product state/dashboard state
	const [products, setProducts] = useState([])

	// Add product states
	const [productName, setProductName] = useState('');
	const [productDescription, setProductDescription] = useState('');
	const [productPrice, setProductPrice] = useState('');
	const [productQuantity, setProductQuantity] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	// Update product states
	const [updateProdName, setUpdateProdName] = useState('');
	const [updateProdDesc, setUpdateProdDesc] = useState('');
	const [updateProdPrice, setUpdateProdPrice] = useState('');
	const [updateProdQuantity, setUpdateProdQuantity] = useState('');
	const [updateProdId, setUpdateProdId] = useState('');


	
	/* FUNCTION TO UPDATE ISACTIVE VALUE - ACTIVATE PRODUCT */
	function activateProduct(id){
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/activate` , {
			method: 'PUT',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				showToast({
					title: 'Successfully Activated Product',
					description: 'Check your products',
					bgcolor: 'success'
				})
			}
		})
	}
	/* END ACTIVATE PRODUCT FUNCTION */

	/* FUNCTION TO UPDATE ISACTIVE VALUE - DEACTIVATE PRODUCT */
	function deactivateProduct(id){
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive` , {
			method: 'PUT',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				showToast({
					title: 'Deactivated Product',
					description: 'Check your products',
					bgcolor: 'danger'
				})
			}
		})
	}
	/* END DEACTIVATE PRODUCT FUNCTION */
	


	// Function to fill dashboard with all products and update when any changes
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(response => response.json())
		.then(data => {
			let count = 0;
			setProducts(data.map(products => {
				count++;
				return(
					<tr key = {products._id}>
					  <td>{count}</td>
  			          <td>{products.name}</td>
  			          <td>{products.description}</td>
  			          <td>{products.price}</td>
  			          <td>{products.quantity}</td>
  			          <td>{products.isActive.toString()}</td>
  			          <td><Button onClick={e => editProduct(products._id)}>Update</Button></td>
			          {
			          	products.isActive === true 
      			          ?
      			          <td><Button variant='danger' onClick={e => deactivateProduct(products._id)}>Deactivate</Button></td>
      			          :
      			          <td><Button variant='success' onClick={e => activateProduct(products._id)}>Activate</Button></td>
      			      }
					</tr>
				)
			}));
		})
	}, [products])	


	/* FUNCTION TO PASS SELECTED PRODUCT DETAILS TO BE UPDATED AND SHOW MODAL TO EDIT*/
	function editProduct(prod){
		fetch(`${process.env.REACT_APP_API_URL}/products/${prod}`)
		.then(response => response.json())
		.then(data => {
		 	showModal(data)
		})
	}
	/* END FUNCTION EDIT PRODUCT DETAILS */
	

	useEffect(() => {
		if(productName != '' && productDescription != '' && productPrice != 0 && productQuantity != 0){
			return setIsDisabled(false);
		}
		return setIsDisabled(true);
	}, [productName, productDescription, productPrice, productQuantity])

	// Function to access route that will add product to db
	function addProduct(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products` , {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name : productName,
				description : productDescription,
				price : productPrice,
				quantity : productQuantity
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				Swal2.fire({
					title: 'Successfully Added Product!',
					icon: 'success',
					text: 'Check your products.'
				})
				closeAddProdModal();
				setProductName('');
				setProductDescription('');
				setProductPrice('');
				setProductQuantity('');
			}
		})
	}

	return(
		user.isAdmin === true
		?

		<Container fluid>
		<Modal size='xl' show={showAddModal} onHide={closeAddProdModal} backdrop="static" keyboard={false} centered>
				<Modal.Header closeButton>
				  <Modal.Title>Add Product</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Container>
					<Form onSubmit={e => addProduct(e)}>
						<Row>
							<Col xl={2}>
							      <Form.Group className="mb-3" controlId="productName">
							        <Form.Control type="text" placeholder="Enter Product Name" value={productName} onChange={(e) => setProductName(e.target.value)}/>
							      </Form.Group>
					      	</Col>
					      	<Col xl={6}>
							      <Form.Group className="mb-3" controlId="productDescription">
							        <Form.Control type="text" placeholder="Enter Description" value={productDescription} onChange={(e) => setProductDescription(e.target.value)}/>
							      </Form.Group>
							</Col>
							<Col xl={1}>
							      <Form.Group className="mb-3" controlId="formBasicPassword2">
							        <Form.Control type="text" placeholder="Price" value={productPrice} onChange={e => setProductPrice(e.target.value)}/>
							      </Form.Group>
							</Col>
							<Col xl={1}>
							      <Form.Group className="mb-3" controlId="formBasicPassword2">
							        <Form.Control type="text" placeholder="Quantity" value={productQuantity} onChange={e => setProductQuantity(e.target.value)}/>
							      </Form.Group>
							</Col>
							<Col xl={2}>
							      <Button variant="primary" type="submit" disabled={isDisabled}>
							        Add Product
							      </Button>
					      	</Col>
						</Row>
					</Form>
					</Container>
			        </Modal.Body>
					<Modal.Footer>
						<Button variant="outline-danger" onClick={closeAddProdModal}>
						Discard Changes
						</Button>
					</Modal.Footer>
				</Modal>
		<Row>
			<Button variant="outline-success" onClick={e => showAddProdModal()}>Add New Product</Button>
			<Col>
				<Table striped>
			      <thead>
			        <tr>
			          <th>#</th>
			          <th>Product Name</th>
			          <th>Description</th>
			          <th>Price</th>
			          <th>Stock</th>
			          <th>Active</th>
			          <th>Actions</th>
			        </tr>
			      </thead>
			      <tbody>
		          	{products}
			      </tbody>
			    </Table>
			</Col>
		</Row>
		</Container>
		:
		<PageNotFound />
	)
}