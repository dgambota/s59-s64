import {Form, Button, Col, Row} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import PageNotFound from '../pages/PageNotFound';

// sweetalert2 is a simple and useful package for generating user alerts with ReactJS
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

	const navigate = useNavigate();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	// Allows us to consume the UserContext obejct and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	// const [user, setUser] = useState(localStorage.getItem('email'))

	useEffect(() => {
		if(email != '' && password != ''){
			return setIsDisabled(false);
		}
		return setIsDisabled(true);
	}, [email, password])

	function login(e){
		e.preventDefault();


		fetch(`${process.env.REACT_APP_API_URL}/users/login` , {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data === false){
				Swal2.fire({
					title: 'Login unsuccessful',
					icon: 'error',
					text: 'Check your login credentials and try again'
				})
			} else {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal2.fire({
					title: 'Login successful',
					icon: 'success',
					text: 'Enjoy shopping with us!'
				})

				navigate('/');
			}
		})
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	return(
		user.id === null || user.id === undefined
		?
		<Row>
			<Col className="col-6 mx-auto">
				<h3>Login</h3>
				<Form onSubmit={e => login(e)}>
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicPassword1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
			      </Form.Group>
			      <Button variant="primary" type="submit" disabled={isDisabled}>
			        Login
			      </Button>
			    </Form>	
			</Col>
		</Row>
		:
		<PageNotFound />
	)
}