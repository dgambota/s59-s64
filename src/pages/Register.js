import {Button, Form, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom'
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';
import PageNotFound from '../pages/PageNotFound'

export default function Register(){
	const navigate = useNavigate();
	const {user} = useContext(UserContext);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isPassed, setIsPassed] = useState(true);
	const [isDisabled, setIsDisabled] = useState(true);
	const [isPasswordMatch, setIsPasswordMatch] = useState(true)
	const [isMobileNumber, setIsMobileNumber] = useState(true);

	useEffect(() => {
		if(email.length > 15){
			return setIsPassed(false);
		}
		return setIsPassed(true);
	}, [email])

	useEffect(() => {
		if(mobileNumber.length < 11 && mobileNumber.length > 0){
			return setIsMobileNumber(false);
		}
		return setIsMobileNumber(true);
	}, [mobileNumber])

	useEffect(() => {
		if(email.length <= 15 && email != '' && password1 != '' && password2 != '' && password1 === password2 && lastName != '' && firstName != '' && mobileNumber.length > 10){
			return setIsDisabled(false);
		}
		return setIsDisabled(true);
	}, [email, password1, password2, lastName, firstName, mobileNumber])

	function registerUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json'
			},
			body: JSON.stringify({
				email : email
			})
		})
		.then(response => response.json())
		.then(data => {
			// if duplicate is found
			if(data === true){
				Swal2.fire({
					title: 'Duplicate Email found',
					icon: 'error',
					text: 'Please provide a different email.'
				})
			} 
			// If no duplicate is found, proceed to register user details
			else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register` , {
					method: 'POST',
					headers: {
						'Content-type' : 'application/json'
					},
					body: JSON.stringify({
						firstName : firstName,
						lastName : lastName,
						email : email,
						mobileNo : mobileNumber,
						password : password2
					})
				})
				.then(response => response.json())
				.then(data => {
					// console.log(data);
					if(data === true){
						Swal2.fire({
							title: 'Registration Successful',
							icon: 'success',
							text: 'You can now login.'
						})
						navigate('/login');
					}
				})
			}
			
		})

		
		
		
	}

	useEffect(() => {
		if(password1 === password2){
			return setIsPasswordMatch(true);
		}
		return setIsPasswordMatch(false);
	}, [password1, password2])

	return(
		user.id === null || user.id === undefined
		?
		<Row>
			<Col className="col-6 mx-auto">
				<h3 className="text-center">Register</h3>
				<Form.Group className="mb-3" controlId="formFirstName">
				  <Form.Label>First Name</Form.Label>
				  <Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={(e) => setFirstName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formLastName">
				  <Form.Label>Last Name</Form.Label>
				  <Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={(e) => setLastName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formMobileNo">
				  <Form.Label>Mobile Number</Form.Label>
				  <Form.Control type="tel" pattern="[0-9]{11}" placeholder="Enter Mobile Number" value={mobileNumber} onChange={(e) => setMobileNumber(e.target.value)}/>
				  <Form.Text className="text-danger" hidden={isMobileNumber}>
				    The mobile number must be 11 digits
				  </Form.Text>
				</Form.Group>

				<Form onSubmit={e => registerUser(e)}>
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)}/>
			        <Form.Text className="text-muted" hidden={isPassed}>
			          The email should not exceed 10 characters
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicPassword1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Password" value={password1} onChange={(e) => setPassword1(e.target.value)}/>
			      </Form.Group>
			      <Form.Group className="mb-3" controlId="formBasicPassword2">
			        <Form.Label>Confirm Password</Form.Label>
			        <Form.Control type="password" placeholder="Re-type Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
			        <Form.Text className="text-danger" hidden={isPasswordMatch}>
			          The password does not match!
			        </Form.Text>
			      </Form.Group>
			      <Button variant="primary" type="submit" disabled={isDisabled}>
			        Sign up
			      </Button>
			    </Form>	
			</Col>
		</Row>
		:
		<PageNotFound />
	)
}